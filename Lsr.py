#!/usr/bin/python
####################
## write for 16s2 9331 assigment2
## by XueYang z3485416

import os, sys, thread, json, time, threading, socket, re, sys, fileinput, heapq

###graph contians nodes except initial node
def findDijkstraPath (Graph, source, destination):
    visited = {}  # boolean value for each node
    distance = {} # minimum sum distance from source to node
    parent = {}   # parent from minimum path back to source
    queue = []        # priority queue implemented with heap on list
    path = []         # final shortest path from source and destion

    # initialise
    for node in Graph.keys():
        visited[node] = False
        distance[node] = float("inf")

    distance[source] = 0
    heapq.heappush(queue, (0, source))

    # main loop
    while queue:
        current = heapq.heappop(queue)[1]
        visited[current] = True

        for connected in Graph[current].keys():
            if not visited[connected]:

                totalDist = distance[current] + Graph[current][connected]
                heapq.heappush(queue, (totalDist, connected))
                # if a shorter distance is found
                if totalDist < distance[connected]:
                    # replace it as the new shortest distance
                    distance[connected] = totalDist
                    parent[connected] = current

    # trace parent from source to find path
    trace = destination
    while trace != source:
        # push on to path list
        path.insert(0, trace)
        trace = parent[trace]

    path.insert(0, source)

    return (path, distance[destination])

def calculate_path(raw_graph):
    global print_count
    for nodes in raw_graph:
        ajacent_nodes = {}
        for ajacent_ID in raw_graph[nodes]:
            # print ajacent_ID
            ajacent_nodes[ajacent_ID[0]] = float(ajacent_ID[1])
        graph[nodes] = ajacent_nodes
    print_count += 1
    print '----------------',print_count,'----------------'
    for node in graph:
        if node != port_name:
            (path, shortestDist) = findDijkstraPath(graph, port_name, node)
            pathf = "".join(path)  # path formatted as string seperated by spaces
            print "least-cost path to node {0}: {1} and the cost is {2}.".format(node, pathf, shortestDist)



def loadfile(config_file_name):
    global graph
    graph = {}
    raw_next_hop_list = []
    packet_to_next_hops = {}
    fo = open(config_file_name)

    for line in fo:
        raw_next_hop_list.append(line.strip('\n').split())

    config_lists[port_name] = raw_next_hop_list[1:]

    packet_to_next_hops[port_name] = raw_next_hop_list[1:]
    json_dump_packet = json.dumps(packet_to_next_hops)

    return raw_next_hop_list[1:],json_dump_packet

def send_config():
    while True:
        for i in range(len(next_hop_list)):
            send_config_socket.sendto(json_dump_packet, ('localhost', int(next_hop_list[i][2])))
        time.sleep(1)
def send_keep_live_message():
    heart_beat = {}
    heart_beat['live'] = port_name
    heart_beat_json_dump = json.dumps(heart_beat)
    while True:
        for i in range(len(next_hop_list)):
            send_config_socket.sendto(heart_beat_json_dump, ('localhost', int(next_hop_list[i][2])))
        time.sleep(0.3)


def recv_config():
    global config_lists
    global config_dic
    raw_tem = {}
    next_hop_nodes = {}
    flag = 0

    for i in next_hop_list:
        next_hop_nodes[i[0]] = 0
        # raw_lis'
    while True:
        second_time_start = time.time()
        while True:
            got_json_dump_packet = receive_config_socket.recvfrom(1024)
            config_dic = json.loads(got_json_dump_packet[0])
            ###############################
            ###############################
            ############down###############
            if config_dic.keys()[0] == 'live':
                count_betas_start_time = time.time()
                if config_dic['live'] in next_hop_nodes:
                    next_hop_nodes[config_dic['live']] = 1

            if time.time() - count_betas_start_time > 1:
                for nodes in next_hop_nodes:
                    print next_hop_nodes,'--------'
                    if next_hop_nodes[nodes] == 0:
                        print 'Failed nodes------',nodes
                        del config_dic[nodes]
            else:
                if config_dic['live'] in next_hop_nodes:
                    next_hop_nodes[config_dic['live']] = 0

            #############up################
            ###############################
            ###############################

            new_dic = {}
            new_dic = config_dic
            config_lists=dict(new_dic, **config_lists)
            try:
                if time.time() - start_time > 15 and flag == 0:
                    #############################
                    #####calculate_path here#####
                    #############################
                    calculate_path(config_lists)
                    flag = 1
                    break
                elif time.time() - second_time_start > 15 and flag == 1:
                    calculate_path(config_lists)
                    break

            except:
                print 'calculate_path error'




def pass_config():
    while True:
        packet_node = config_dic.keys()
        if len(packet_node)>0:
            if packet_node not in passed_nodes:
                # print packet_node,'@@@@@',passed_nodes,'@@@@@@@',config_dic
                json_dump_packet = json.dumps(config_dic)
                next_nodes = []
                time.sleep(1)
                for node in next_hop_list:
                    send_config_socket.sendto(json_dump_packet, ('localhost', int(node[2])))
                passed_nodes.append(packet_node)


def start_threading():
    threading.Thread(target=recv_config).start()
    threading.Thread(target=send_keep_live_message).start()
    threading.Thread(target=send_config).start()
    threading.Thread(target=pass_config).start()


if __name__  == '__main__':
    try:
        port_name = sys.argv[1]
        port_number = int(sys.argv[2])
        config_file_name = sys.argv[3]
        if port_number < 1024:
            raise Exception
    except:
        print('port number should be great than 1024.')
        sys.exit()

    try:
        send_config_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except socket.error:
        print 'Failed to create socket'
        sys.exit()
    try:
        receive_config_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except socket.error, msg:
        print 'Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()
    try:
        receive_config_socket.bind(('localhost', port_number))
    except socket.error, msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    global config_lists
    global next_hop_list
    global next_hop_nodes_list
    global passed_nodes

    passed_nodes = []
    next_hop_nodes_list = []
    config_dic = {}
    config_lists = {}
    print_count = 0
    start_time = time.time()

    next_hop_list,json_dump_packet = loadfile(config_file_name)
    print next_hop_list,'next_hop_list'

    for info in next_hop_list:
        next_hop_nodes_list.append(info[0])
    # print '000000000',next_hop_nodes_list


    start_threading()

#!/usr/bin/env python

def dijkstra(graph,src,dest,visited=[],distances={},predecessors={}):
    """ calculates a shortest path tree routed in src
    """
    # a few sanity checks
    if src not in graph:
        raise TypeError('the root of the shortest path tree cannot be found in the graph')
    if dest not in graph:
        raise TypeError('the target of the shortest path cannot be found in the graph')
    # ending condition
    if src == dest:
        # We build the shortest path and display it
        path=[]
        pred=dest
        while pred != None:
            path.append(pred)
            pred=predecessors.get(pred,None)
        print('shortest path: '+str(path)+" cost="+str(distances[dest]))
    else :
        # if it is the initial  run, initializes the cost
        if not visited:
            distances[src]=0
        # visit the neighbors
        for neighbor in graph[src] :
            if neighbor not in visited:
                new_distance = distances[src] + graph[src][neighbor]
                if new_distance < distances.get(neighbor,float('inf')):
                    distances[neighbor] = new_distance
                    predecessors[neighbor] = src
        # mark as visited
        visited.append(src)
        # now that all neighbors have been visited: recurse
        # select the non visited node with lowest distance 'x'
        # run Dijskstra with src='x'
        unvisited={}
        for k in graph:
            if k not in visited:
                unvisited[k] = distances.get(k,float('inf'))
        x=min(unvisited, key=unvisited.get)
        dijkstra(graph,x,dest,visited,distances,predecessors)



# if __name__ == "__main__":
#     #import sys;sys.argv = ['', 'Test.testName']
#     #unittest.main()
#     graph = {
# 	'A': {'B': 2,'C':5,'D':1},
# 	'B': {'A': 2,'C':3,'D':2},
# 	'C': {'A': 5,'B':3,'D':3,'E':1,'F':5},
# 	'D': {'A': 1,'B':2,'C':3,'E': 1},
# 	'E': {'C': 1,'D':1,'F':2},
# 	'F': {'C': 5,'E':2},
# 	}
    dijkstra(graph,'A','C')
# And magic... It returns:
# shortest path: ['t', 'd', 'b', 's'] cost=8

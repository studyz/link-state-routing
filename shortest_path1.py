#!/usr/bin/env python

# Program to find shortest path using Dijkstra's Algorithm
# Callum Howard 2014 - COMP2041

import sys, fileinput, heapq

# returns list with shortest path using Dijkstra
def findDijkstraPath (Graph, source, destination):

    # declare
    visited = {}  # boolean value for each node
    distance = {} # minimum sum distance from source to node
    parent = {}   # parent from minimum path back to source
    queue = []        # priority queue implemented with heap on list
    path = []         # final shortest path from source and destion

    # initialise
    for node in Graph.keys():
        visited[node] = False
        distance[node] = float("inf")

    distance[source] = 0
    heapq.heappush(queue, (0, source))

    # main loop
    while queue:
        current = heapq.heappop(queue)[1]
        visited[current] = True

        for connected in Graph[current].keys():
            if not visited[connected]:
                totalDist = distance[current] + Graph[current][connected]
                heapq.heappush(queue, (totalDist, connected))

                # if a shorter distance is found
                if totalDist < distance[connected]:
                    # replace it as the new shortest distance
                    distance[connected] = totalDist
                    parent[connected] = current

    # trace parent from source to find path
    trace = destination
    while trace != source:
        # push on to path list
        path.insert(0, trace)
        trace = parent[trace]

    path.insert(0, source)

    return (path, distance[destination])


if __name__  == '__main__':
    # initial_node = {'A': {'B': 2,'C':5,'D':1}}
    try:
        source = sys.argv[1]
        destination = sys.argv[2]
    except:
        sys.exit()
        #
    # nodes = ('A', 'B', 'C', 'D', 'E', 'F')
    graph = {
    'A':{'B':2,'C':5,'D':1},
    'B': {'A': 2,'C':3,'D':2},
    'C': {'A': 5,'B':3,'D':3,'E':1,'F':5},
    'D': {'A': 1,'B':2,'C':3,'E': 1},
    'E': {'C': 1,'D':1,'F':2},
    'F': {'C': 5,'E':2}
    }
    # calculate shortest route
    (path, shortestDist) = findDijkstraPath(graph, source, destination)

    # print result
    pathf = "".join(path)  # path formatted as string seperated by spaces
    print "least-cost path to node {0}: {1} and the cost is {2}.".format(destination, pathf, shortestDist)
